Summary: A lazy "hello world"
Name: hi
Version: %{VERSION}
Release: %{RELEASE}
License: GPL
Packager: open*i
Source: file:///home/pking/projects/openi/3536/hi.tgz
URL: file:///home/pking/projects/openi/3536/README.md
%description
A "hello world" so lazy, it only says "Hi!".  At least it's friendly.

%prep
tar -xzf "${RPM_SOURCE_DIR}/hi.tgz" --strip-components=1 ;

%build
gcc -static -o hi hi.c ;

%install
mkdir -p "%{buildroot}/usr/local/bin" ;
install -s hi "%{buildroot}/usr/local/bin/hi" ;

%files
"/usr/local/bin/hi"
